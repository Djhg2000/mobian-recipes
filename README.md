# D2K fork of Mobian

This is my personal test platform for Mobian development and contains a few new
features over upstream Mobian, including but not limited to:

- Modified keyboard layouts
- GPS support through gpsd (no gpsOneXTRA, aka AGPS support)
- More complete CLI environment

As of 2020-06-15 I'm using it as my daily driver distribution. In my experience
so far calls and SMS work reliably.

## Why did you fork Mobian instead of contributing upstream?

Some of these changes are not suitable for end users. Most people will never use
the extra development tools provided in this fork so Mobian is a better fit for
general use.

This just goes to show how Mobian makes a good base for making the operating
system you want instead of simply being one of the provided operating systems. I
originally intended to work on UBPorts but as I'm already very familiar with
Debian this was the path of least resistance.

## How do I upgrade?

Since I don't run my own repository you need to flash every new build and start
over. Don't worry, the successful changes will peobably make their way back into
Mobian soon.

## Known issues

For a complete list see the open issues page, but here's a quick rundown:

- Automatic brightness curve is inverted (upstream?)
- Locking the device doesn't immediately trigger the lock screen (upstream?)
- Autorotation doesn't work (upstream bug)
- No AGPS means long GPS cold boot times (AFAICT the same goes for UBPorts)
- Native applications start windowed instead of fullscreened (upstream?)
- Contacts without a country prefix will get the +1 prefix in dialer (upstream?)

Known issues labeled with "(upstream?)" are probably upstream but its origin is
still undetermined.

## Ongoing experiments

### Cedrus

Needs patching for 64 bit. Current upstream source depends on 32 bit assembly
for fast conversion between tiled and planar surface formats. Doesn't look too
hard to port (short and well commented) but I'll have to learn some ARM assembly
first.

### AGPS

ModemManager grabs the AT serial port. We can either patch ModemManager to
supply xtra.bin to the modem directly (beyond my current skills and patience,
look at the source and you'll see what I mean) or we can figure out a
workaround. Something like injecting it on boot through a systemd script before
ModemManager is even started sounds feasable enough.

Needs documentation.

### kgx (King's Cross Terminal)

I want a better font. Preferably one that makes 80 columns fit without becoming
unreadable. Probably needs some source code hacking.

### Matrix client with E2E support

I need this for day-to-day use. Riot.im in Firefox ESR works but is hardly a
long term solution. Mirage works very well on my main desktop but it would need
recompiling and packaging for ARM. Other suggestions are welcome.

### Keyboard layouts

Keyboard layouts can be further improved. A smaller number row at the top and
left/right arrow keys next to the space bar for regular keyboards would increase
usability a lot. Stretching the keys to the edges like I did with the terminal
keyboard would also help.

## Planned experiments

### Autorotation

Not that difficult, patches are staged for merging upstream so we can just take
those and recompile Phosh.

### Automatic brightness curve

When the inversion issue is fixed we might need to tweak the curve to maintain a
reasonably consistent apparent brightness.

### Video player

When Cedrus works it's time to pick a video player with Cedrus support. Current
candidates for investigation are VLC and Totem (the native GNOME video player).

### Angelfish browser

Needs Qt 5.13 but we only have Qt 5.12 in Mobian. We could wait for upstream or
recompile Qt.
